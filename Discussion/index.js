// Use the "require" directive to load the express module/package
const express = require("express");

// Create an application using express
const app = express(); 

// For our application server to run, we need a port to listen to
const port = 3000;

// Methods used from express JS are middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system

// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
// {extended:true} allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));


// [SECTION] Routes
// Express has methods corresponding to each HTTP method

//GET
// This route expects to receive a GET request at the URI "/hello"
app.get("/greet", (request, response) => {
	 // response.send uses the express.js module's method to send a response back to the client
	response.send("Hello from the /greet endpoint!");
});


// POST
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (request, response) => {
    // request.body contains the contents/data of the request body
    response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


// Simple registration form
// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database
let users = [];

// This route expects to receive a POST request at the URI "/signup"
// This will create a user object in the "users" variable that mirrors a real world registration process
app.post("/signup", (request, response) => {

    // If contents of the "request body" with the property "username" and "password" is not empty
    if(request.body.username !== '' && request.body.password !== ''){

        // This will store the user object sent via Postman to the users array created above
        users.push(request.body);

        // This will send a response back to the client/Postman after the request has been processed
        response.send(`User ${request.body.username} successfully registered!`);

    // If the username and password are not complete an error message will be sent back to the client/Postman
    } else {

        response.send("Please input BOTH username and password.");

    }

});


// Change password
app.patch("/change-password", (request, response) => {

    // Creates a variable to store the message to be sent back to the client
    let message;

    // If the username provided in the Postman and thhe username of the current object in the loop is the same
    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username) {
            // Change the password
            users[i].password = request.body.password;

            // message to be sent back if password has been updated successfully
            message = `User ${request.body.username}'s password has been updated.`;
            break;
            // if no username match
        } else {
            message = "User does not exist.";
        }
    }
    response.send(message);
});


// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));
