const express = require('express');

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}))

let users = [
	{
		username: "johndoe",
		password: "johndoe1234"
	}
]; 

app.get("/home", (request, response) => {
	response.send("Welcome to the home page");
});

app.get("/users", (request, response) => {
	response.send(users);
});

app.delete("/delete-user", (request, response) => {

	let message = "User does not exist.";

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			 message = `User ${users[i].username} has been deleted.`;
			users.pop(i);
			break;
		}
	}
	response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
